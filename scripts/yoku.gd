extends StaticBody2D

var is_active = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_active:
		active_state()
	else:
		inactive_state()

func active_state():
	$sprite.frame = 35
	$CollisionShape2D.disabled = false

func inactive_state():
	$sprite.frame = 34
	$CollisionShape2D.disabled = true