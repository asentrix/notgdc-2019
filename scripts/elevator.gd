extends KinematicBody2D

const SPEED = 50

var go_up = false
var motion = Vector2()
export var going_vert = true

# X
export var min_x = 0
export var max_x = 0

# Y
export var min_y = 0
export var max_y = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if going_vert:
		move_vertical()
	else:
		move_horizontal()
		
	motion = move_and_slide(motion, Vector2())

func move_vertical():
	if position.y <= min_y:
		go_up = false
	elif position.y >= max_y:
		go_up = true
	
	if go_up:
		motion.y = -SPEED
	else:
		motion.y = SPEED

func move_horizontal():
	if position.x > min_x:
		motion.x = -SPEED
	elif position.x < max_x:
		motion.x = SPEED
